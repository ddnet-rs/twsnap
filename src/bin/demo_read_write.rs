use std::env;
use std::error::Error;
use std::fs;
use std::process;
use twsnap::compat::ddnet;
use twsnap::Snap;

fn next_arg(args: &mut env::Args, program: &str) -> String {
    match args.next() {
        Some(arg) => arg,
        None => {
            eprintln!("Usage: {} INPUT_DEMO OUTPUT_PATH", program);
            process::exit(-1);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = env::args();
    let program_name = next_arg(&mut args, "<cli-executable>");
    let input_path = next_arg(&mut args, &program_name);
    let output_path = next_arg(&mut args, &program_name);

    let input_file = fs::File::open(input_path)?;
    let mut reader = ddnet::DemoReader::new(input_file)?;
    let output_file = fs::File::create(output_path)?;
    let mut writer = ddnet::DemoWriter::new(
        output_file,
        reader.kind(),
        reader.timestamp(),
        reader.net_version(),
        reader.map_name(),
        reader.map_data(),
        reader.map_hash(),
        reader.length(),
    )?;
    let mut snap = Snap::default();
    while let Some(chunk) = reader.next_chunk(&mut snap)? {
        match chunk {
            ddnet::DemoChunk::Snapshot(tick) => writer.write_snapshot(tick, &snap)?,
            ddnet::DemoChunk::NetMsg => {}
        }
    }

    Ok(())
}
