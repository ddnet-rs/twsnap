//! All the items that are in a server snapshot

use crate::time::{Duration, Instant};
use crate::{enums, flags, AnglePrecision, Position, SkinColor, SnapId, Velocity};
use arrayvec::ArrayString;
use serde::{Deserialize, Serialize};
use vek::Vec2;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct GameInfo {
    pub flags: flags::GameFlags,
    pub game_state_flags: flags::GameStateFlags,
    pub round_start_tick: Instant,
    pub warmup_timer: Duration,
    pub score_limit: i32,
    /// duration from zero means no time limit
    pub time_limit: Duration,
    pub round_num: i32,
    pub round_current: i32,
    // GameInfoEx
    pub flags_ex: flags::GameFlagsEx,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Laser {
    pub to: Position,
    pub from: Position,
    pub start_tick: Instant,
    // none for map laser
    pub owner: Option<SnapId>,
    // rifle, shotgun normal laser, other map laser
    pub kind: enums::LaserType,
    pub switch_number: u8,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct MapProjectile {
    pub pos: Position,
    /// normalized * 100.0
    pub direction: Vec2<i32>,
    pub kind: enums::ActiveWeapon,
    pub start_tick: Instant,
    // DdnetProjectile
    pub switch_number: u8,
    pub tune_zone: u8,
    pub flags: flags::ProjectileFlags,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Pickup {
    pub pos: Position,
    pub kind: enums::Powerup,
    // DdnetPickup
    pub switch_number: u8,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Player {
    // ClientInfo
    pub name: ArrayString<15>,
    pub clan: ArrayString<11>,
    pub country: i32,
    pub skin: ArrayString<23>,
    pub use_custom_color: bool,
    pub color_body: SkinColor,
    pub color_feet: SkinColor,

    // DDNetPlayer
    pub flags: flags::PlayerFlags,
    pub auth_level: enums::Authed,
    pub team: i32,

    // PlayerInfo
    /// whether the player is the one the snap was created for
    pub local: bool,
    pub teeworlds_team: enums::ClientTeam,
    pub score: i32,
    pub latency: i32,

    // All Character-related items
    pub tee: Option<Tee>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Projectile {
    pub pos: Position,
    /// normalized * 100.0
    pub direction: Vec2<i32>,
    pub kind: enums::ActiveWeapon,
    pub start_tick: Instant,
    pub owner: SnapId,
    pub tune_zone: u8,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct PvpFlag {
    pub pos: Position,
    pub pvp_team: enums::PvpTeam,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Tee {
    // CharacterCore
    pub tick: Instant,
    pub pos: Position,
    pub vel: Velocity,

    /// Angle is in radians
    pub angle: AnglePrecision,
    pub direction: enums::Direction,

    pub jumped: flags::JumpFlags,
    pub hooked_player: Option<SnapId>,
    pub hook_state: enums::HookState,
    /// Despite this named `_tick` this variable actually contains a duration
    /// In a future version this would be better made an `Instant` to keep diffs smaller.
    pub hook_tick: Duration,

    /// **Absolute** position of hook end point
    pub hook_pos: Position,
    /// normalized direction the hook is moving
    pub hook_direction: Velocity,

    // Character
    pub flags: flags::TeeFlags, // also contains flags from DDNetCharacter
    pub health: i32,
    pub armor: i32,
    pub ammo_count: i32,
    pub weapon: enums::ActiveWeapon,
    pub emote: enums::Emote,
    pub attack_tick: Instant,

    // DDNetCharacter
    pub freeze_end: Instant,
    pub jumps: i32,
    pub tele_checkpoint: i32,
    pub strong_weak_id: i32,
    pub jumped_total: i32,
    pub ninja_activation_tick: Instant,
    pub freeze_start: Instant,
    // cursor target
    pub target: Position,
}
