pub struct ClSay<'a> {
    pub mode: Chat,
    pub target: i32,
    pub message: &'a [u8],
}