use bitflags::bitflags;
use serde::{Deserialize, Serialize};

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct GameFlags: i32 {
        const TEAMS = 1<<0;
        const FLAGS = 1<<1;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct GameFlagsEx: u64 {
        const TIMESCORE = 1<<0;
        const GAMETYPE_RACE = 1<<1;
        const GAMETYPE_FASTCAP = 1<<2;
        const GAMETYPE_FNG = 1<<3;
        const GAMETYPE_DDRACE = 1<<4;
        const GAMETYPE_DDNET = 1<<5;
        const GAMETYPE_BLOCK_WORLDS = 1<<6;
        const GAMETYPE_VANILLA = 1<<7;
        const GAMETYPE_PLUS = 1<<8;
        const FLAG_STARTS_RACE = 1<<9;
        const RACE = 1<<10;
        const UNLIMITED_AMMO = 1<<11;
        const DDRACE_RECORD_MESSAGE = 1<<12;
        const RACE_RECORD_MESSAGE = 1<<13;
        const ALLOW_EYE_WHEEL = 1<<14;
        const ALLOW_HOOK_COLL = 1<<15;
        const ALLOW_ZOOM = 1<<16;
        const BUG_DDRACE_GHOST = 1<<17;
        const BUG_DDRACE_INPUT = 1<<18;
        const BUG_FNG_LASER_RANGE = 1<<19;
        const BUG_VANILLA_BOUNCE = 1<<20;
        const PREDICT_FNG = 1<<21;
        const PREDICT_DDRACE = 1<<22;
        const PREDICT_DDRACE_TILES = 1<<23;
        const PREDICT_VANILLA = 1<<24;
        const ENTITIES_DDNET = 1<<25;
        const ENTITIES_DDRACE = 1<<26;
        const ENTITIES_RACE = 1<<27;
        const ENTITIES_FNG = 1<<28;
        const ENTITIES_VANILLA = 1<<29;
        const DONT_MASK_ENTITIES = 1<<30;
        const ENTITIES_BW = 1<<31;
        // GameInfoFlags2
        const ALLOW_X_SKINS = 1<<32;
        const GAMETYPE_CITY = 1<<33;
        const GAMETYPE_FDDRACE = 1<<34;
        const ENTITIES_FDDRACE = 1<<35;
        const HUD_HEALTH_ARMOR = 1<<36;
        const HUD_AMMO = 1<<37;
        const HUD_DDRACE = 1<<38;
        const NO_WEAK_HOOK = 1<<39;
        const NO_SKIN_CHANGE_FOR_FROZEN = 1<<40;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct GameStateFlags: i32 {
        const GAMEOVER = 1<<0;
        const SUDDENDEATH = 1<<1;
        const PAUSED = 1<<2;
        const RACETIME = 1<<3;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct JumpFlags: i32 {
        /// `m_Jumped & 1`, determines whether feet are greyed out
        const USED_JUMP_INPUT = 1<<0;
        const ALL_AIR_JUMPS_USED =  1<<1;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct LaserFlags: i32 {
        const NO_PREDICT = 1<<0;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct PlayerFlags: i32 {
        // DDNetPlayer flags (EXPLAYERFLAG)
        const AFK = 1<<0;
        const PAUSED = 1<<1;
        const SPEC = 1<<2;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct ProjectileFlags: i32 {
        const BOUNCE_HORIZONTAL = 1<<0;
        const BOUNCE_VERTICAL = 1<<1;
        const EXPLOSIVE = 1<<2;
        const FREEZE = 1<<3;
        const NORMALIZE_VEL = 1<<4;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct TeeFlags: u64 {
        const SOLO = 1<<0;
        const JETPACK = 1<<1;
        const COLLISION_DISABLED = 1<<2;
        const ENDLESS_HOOK = 1<<3;
        const INFINITE_JUMPS = 1<<4;
        const SUPER = 1<<5;
        const HAMMER_HIT_DISABLED = 1<<6;
        const SHOTGUN_HIT_DISABLED = 1<<7;
        const GRENADE_HIT_DISABLED = 1<<8;
        const LASER_HIT_DISABLED = 1<<9;
        const HOOK_HIT_DISABLED = 1<<10;
        const TELEGUN_GUN = 1<<11;
        const TELEGUN_GRENADE = 1<<12;
        const TELEGUN_LASER = 1<<13;
        const WEAPON_HAMMER = 1<<14;
        const WEAPON_GUN = 1<<15;
        const WEAPON_SHOTGUN = 1<<16;
        const WEAPON_GRENADE = 1<<17;
        const WEAPON_LASER = 1<<18;
        const WEAPON_NINJA = 1<<19;
        const MOVEMENTS_DISABLED = 1<<20;
        const IN_FREEZE = 1<<21;
        const PRACTICE_MODE = 1<<22;

        // PlayerFlags
        const PLAYING = 1<<32;
        const IN_MENU = 1<<33;
        const CHATTING = 1<<34;
        const SCOREBOARD = 1<<35;
        const AIM_LINE = 1<<36;
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct GotWeapon: u8 {
        const HAMMER =  0b0000_0001;
        const PISTOL =  0b0000_0010;
        const SHOTGUN = 0b0000_0100;
        const GRENADE = 0b0000_1000;
        const RIFLE =   0b0001_0000;
        const NINJA =   0b0010_0000;
    }
}
