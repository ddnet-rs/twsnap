use crate::items::{GameInfo, Laser, MapProjectile, Pickup, Player, Projectile, PvpFlag};
use crate::{events, OrderedMap};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Events {
    DamageIndicator(events::DamageIndicator),
    Death(events::Death),
    Explosion(events::Explosion),
    HammerHit(events::HammerHit),
    Sound(events::Sound),
    SoundGlobal(events::SoundGlobal),
    Spawn(events::Spawn),
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Snap {
    // Snap objects
    pub game_infos: OrderedMap<GameInfo>,
    pub lasers: OrderedMap<Laser>,
    pub map_projectiles: OrderedMap<MapProjectile>,
    pub pickups: OrderedMap<Pickup>,
    pub players: OrderedMap<Player>,
    pub projectiles: OrderedMap<Projectile>,
    pub pvp_flags: OrderedMap<PvpFlag>,
    // snap events
    pub events: Vec<Events>,
}

impl Snap {
    pub fn clear(&mut self) {
        // clear snap objects
        self.game_infos.clear();
        self.lasers.clear();
        self.map_projectiles.clear();
        self.pickups.clear();
        self.players.clear();
        self.projectiles.clear();
        self.pvp_flags.clear();
        // clear events
        self.events.clear();
    }
}
