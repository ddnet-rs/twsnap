use crate::flags::GotWeapon;
use num_enum::{FromPrimitive, IntoPrimitive};
use serde::{Deserialize, Serialize};

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum Emote {
    #[default]
    Normal,
    Pain,
    Happy,
    Surprise,
    Angry,
    Blink,
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Powerup {
    #[default]
    Health,
    Armor,
    Weapon(CollectableWeapon),
    Shield(CollectableWeapon),
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum Emoticon {
    Oop,
    Exclamation,
    #[default]
    Hearts,
    Drop,
    Dotdot,
    Music,
    Sorry,
    Ghost,
    Sushi,
    Splattee,
    Deviltee,
    Zomg,
    Zzz,
    Wtf,
    Eyes,
    Question,
}

#[repr(i32)]
#[derive(
    Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize, IntoPrimitive,
)]
pub enum Authed {
    #[default]
    No,
    Helper,
    Mod,
    Admin,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum Entityclass {
    #[default]
    Projectile,
    Door,
    DraggerWeak,
    DraggerNormal,
    DraggerStrong,
    GunNormal,
    GunExplosive,
    GunFreeze,
    GunUnfreeze,
    Light,
    Pickup,
}

#[repr(i32)]
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum LaserType {
    // Tee gun lasers
    #[default]
    Rifle,
    Shotgun,
    // Map lasers only
    Door,
    Freeze,
    Dragger(LaserDraggerType),
    Gun(LaserGunType),
    Plasma,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum LaserDraggerType {
    Weak,
    WeakNw,
    #[default]
    Normal,
    NormalNw,
    Strong,
    StrongNw,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum LaserGunType {
    #[default]
    Unfreeze,
    Explosive,
    Freeze,
    Expfreeze,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum ActiveWeapon {
    #[default]
    Hammer,
    Pistol,
    Shotgun,
    Grenade,
    Rifle,
    Ninja,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum CollectableWeapon {
    #[default]
    Shotgun = 2,
    Grenade,
    Rifle,
    Ninja,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum SelectableWeapon {
    #[default]
    Hammer,
    Pistol,
    Shotgun,
    Grenade,
    Rifle,
}

impl From<ActiveWeapon> for Option<SelectableWeapon> {
    fn from(weapon: ActiveWeapon) -> Self {
        match weapon {
            ActiveWeapon::Hammer => Some(SelectableWeapon::Hammer),
            ActiveWeapon::Pistol => Some(SelectableWeapon::Pistol),
            ActiveWeapon::Shotgun => Some(SelectableWeapon::Shotgun),
            ActiveWeapon::Grenade => Some(SelectableWeapon::Grenade),
            ActiveWeapon::Rifle => Some(SelectableWeapon::Rifle),
            ActiveWeapon::Ninja => None,
        }
    }
}

impl From<SelectableWeapon> for ActiveWeapon {
    fn from(weapon: SelectableWeapon) -> Self {
        match weapon {
            SelectableWeapon::Hammer => ActiveWeapon::Hammer,
            SelectableWeapon::Pistol => ActiveWeapon::Pistol,
            SelectableWeapon::Shotgun => ActiveWeapon::Shotgun,
            SelectableWeapon::Grenade => ActiveWeapon::Grenade,
            SelectableWeapon::Rifle => ActiveWeapon::Rifle,
        }
    }
}

impl From<CollectableWeapon> for GotWeapon {
    fn from(collectable_weapon: CollectableWeapon) -> Self {
        match collectable_weapon {
            CollectableWeapon::Shotgun => GotWeapon::SHOTGUN,
            CollectableWeapon::Grenade => GotWeapon::GRENADE,
            CollectableWeapon::Rifle => GotWeapon::RIFLE,
            CollectableWeapon::Ninja => GotWeapon::NINJA,
        }
    }
}

impl From<CollectableWeapon> for ActiveWeapon {
    fn from(collectable_weapon: CollectableWeapon) -> Self {
        match collectable_weapon {
            CollectableWeapon::Shotgun => ActiveWeapon::Shotgun,
            CollectableWeapon::Grenade => ActiveWeapon::Grenade,
            CollectableWeapon::Rifle => ActiveWeapon::Rifle,
            CollectableWeapon::Ninja => ActiveWeapon::Ninja,
        }
    }
}

impl PartialEq<CollectableWeapon> for ActiveWeapon {
    fn eq(&self, other: &CollectableWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl PartialEq<SelectableWeapon> for ActiveWeapon {
    fn eq(&self, other: &SelectableWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl PartialEq<ActiveWeapon> for CollectableWeapon {
    fn eq(&self, other: &ActiveWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl PartialEq<SelectableWeapon> for CollectableWeapon {
    fn eq(&self, other: &SelectableWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl PartialEq<ActiveWeapon> for SelectableWeapon {
    fn eq(&self, other: &ActiveWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl PartialEq<CollectableWeapon> for SelectableWeapon {
    fn eq(&self, other: &CollectableWeapon) -> bool {
        *self as u8 == *other as u8
    }
}

impl CollectableWeapon {
    pub fn to_pickup_sound(self) -> Sound {
        match self {
            CollectableWeapon::Shotgun => Sound::PickupShotgun,
            CollectableWeapon::Grenade => Sound::PickupGrenade,
            CollectableWeapon::Rifle => Sound::PickupShotgun,
            CollectableWeapon::Ninja => Sound::PickupNinja,
        }
    }
}

impl SelectableWeapon {
    pub fn next(self) -> Self {
        match self {
            SelectableWeapon::Hammer => SelectableWeapon::Pistol,
            SelectableWeapon::Pistol => SelectableWeapon::Shotgun,
            SelectableWeapon::Shotgun => SelectableWeapon::Grenade,
            SelectableWeapon::Grenade => SelectableWeapon::Rifle,
            SelectableWeapon::Rifle => SelectableWeapon::Hammer,
        }
    }

    pub fn prev(self) -> Self {
        match self {
            SelectableWeapon::Hammer => SelectableWeapon::Rifle,
            SelectableWeapon::Pistol => SelectableWeapon::Hammer,
            SelectableWeapon::Shotgun => SelectableWeapon::Pistol,
            SelectableWeapon::Grenade => SelectableWeapon::Shotgun,
            SelectableWeapon::Rifle => SelectableWeapon::Grenade,
        }
    }

    // TODO: implement TryFrom instead?
    pub fn from_wanted_weapon(from: i32) -> Option<SelectableWeapon> {
        match from {
            1 => Some(SelectableWeapon::Hammer),
            2 => Some(SelectableWeapon::Pistol),
            3 => Some(SelectableWeapon::Shotgun),
            4 => Some(SelectableWeapon::Grenade),
            5 => Some(SelectableWeapon::Rifle),
            _ => None,
        }
    }

    pub fn from_save(from: i32) -> Option<SelectableWeapon> {
        match from {
            0 => Some(SelectableWeapon::Hammer),
            1 => Some(SelectableWeapon::Pistol),
            2 => Some(SelectableWeapon::Shotgun),
            3 => Some(SelectableWeapon::Grenade),
            4 => Some(SelectableWeapon::Rifle),
            _ => None,
        }
    }

    pub fn is_collectable(self) -> bool {
        matches!(
            self,
            SelectableWeapon::Shotgun | SelectableWeapon::Grenade | SelectableWeapon::Rifle
        )
    }
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum Team {
    Spectators = -1,
    #[default]
    Red,
    Blue,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum Sound {
    GunFire,
    ShotgunFire,
    GrenadeFire,
    HammerFire,
    HammerHit,
    NinjaFire,
    GrenadeExplode,
    NinjaHit,
    RifleFire,
    RifleBounce,
    WeaponSwitch,
    PlayerPainShort,
    PlayerPainLong,
    BodyLand,
    PlayerAirjump,
    PlayerJump,
    PlayerDie,
    PlayerSpawn,
    PlayerSkid,
    TeeCry,
    HookLoop,
    HookAttachGround,
    HookAttachPlayer,
    HookNoattach,
    PickupHealth,
    PickupArmor,
    PickupGrenade,
    PickupShotgun,
    PickupNinja,
    WeaponSpawn,
    WeaponNoammo,
    Hit,
    ChatServer,
    ChatClient,
    ChatHighlight,
    CtfDrop,
    CtfReturn,
    CtfGrabPl,
    CtfGrabEn,
    CtfCapture,
    #[default]
    Menu,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum HookState {
    Retracted = -1,
    #[default]
    Idle,
    // Transitional state. Switches to Retracting
    RetractStart,
    // Transitional state. Switches to RetractEnd
    Retracting,
    // Transitional state. Switches to Retracted
    RetractEnd,
    Flying,
    Grabbed,
}

#[repr(i32)]
#[derive(
    Clone, Copy, Debug, Default, PartialEq, Eq, Hash, Serialize, Deserialize, IntoPrimitive,
)]
pub enum Direction {
    /// everything smaller than 0 gets mapped to left
    Left = -1,
    #[default]
    None,
    /// everything greater than 1 gets mapped to right
    Right,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum ClientTeam {
    Spectator = -1,
    #[default]
    Red,
    Blue,
}

#[repr(i32)]
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    Eq,
    Hash,
    Serialize,
    Deserialize,
    FromPrimitive,
    IntoPrimitive,
)]
pub enum PvpTeam {
    #[default]
    Red,
    Blue,
}

// trivial implementations
impl From<i32> for Authed {
    fn from(value: i32) -> Self {
        match value {
            x if x <= Authed::No as i32 => Authed::No,
            x if x == Authed::Helper as i32 => Authed::Helper,
            x if x == Authed::Mod as i32 => Authed::Mod,
            x if x >= Authed::Admin as i32 => Authed::Admin,
            _ => Authed::No,
        }
    }
}

impl From<i32> for Direction {
    fn from(value: i32) -> Self {
        match value {
            x if x <= Direction::Left as i32 => Direction::Left,
            x if x == Direction::None as i32 => Direction::None,
            x if x >= Direction::Right as i32 => Direction::Right,
            _ => Direction::None,
        }
    }
}
