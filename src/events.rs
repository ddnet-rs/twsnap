use crate::{enums, AnglePrecision, Events, Position, SnapId};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DamageIndicator {
    pub pos: Position,
    pub angle: AnglePrecision,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Death {
    pub pos: Position,
    pub player: SnapId,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Explosion {
    pub pos: Position,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct HammerHit {
    pub pos: Position,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Sound {
    pub pos: Position,
    pub sound: enums::Sound,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SoundGlobal {
    pub sound: enums::Sound,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Spawn {
    pub pos: Position,
}

impl From<DamageIndicator> for Events {
    fn from(value: DamageIndicator) -> Self {
        Self::DamageIndicator(value)
    }
}

impl From<Death> for Events {
    fn from(value: Death) -> Self {
        Self::Death(value)
    }
}

impl From<Explosion> for Events {
    fn from(value: Explosion) -> Self {
        Self::Explosion(value)
    }
}

impl From<HammerHit> for Events {
    fn from(value: HammerHit) -> Self {
        Self::HammerHit(value)
    }
}

impl From<Sound> for Events {
    fn from(value: Sound) -> Self {
        Self::Sound(value)
    }
}

impl From<SoundGlobal> for Events {
    fn from(value: SoundGlobal) -> Self {
        Self::SoundGlobal(value)
    }
}

impl From<Spawn> for Events {
    fn from(value: Spawn) -> Self {
        Self::Spawn(value)
    }
}
