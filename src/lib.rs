//! # Snapshot reprentation
//!
//! Goals:
//!  - Can be used to render
//!  - Can be produced by game implementation
//!  - Can be serialized into DDNet demo
//!  - Can be deserialized from DDNet demo
//!  - Can be used as input from game implementation
//!
//!  - valid utf-8
//!  - no allocations
//!  - usability first
//!
//! Goals, if it doesn't affect the representation, otherwise another snapshot is necessary
//!
//!  - Can be deserialized from Teehistorian
//!
//! Non-goals:
//!
//!  - repr(C) to serialize
//!  - Implement functions on snap objects like trajectory.
//!  - no_std
//!  - Serialization can be played by old DDNet clients or Teeworlds Client
//!  - resemblance of [i32; k] strings by Teeworlds
//!  - writing non-DDNet snapshots (other mods)
//!
//!  Architecture:
//!
//!  - Using `fixed` to represent uniform scales for all distance related variables: 1 per tile
//!  - vek for points
//!  - arrayvec for stack strings/arrays
//!  - rust enum for protocol enums
//!  - bitflags
//!  - bools
//!  - One snapshot per team

mod collection;
pub mod compat;
pub mod enums;
pub mod events;
pub mod flags;
pub mod items;
mod snap;
pub mod time;
mod types;

pub use collection::*;
pub use snap::*;
pub use types::*;
